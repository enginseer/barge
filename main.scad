w=100; // ширина поплавка
3dth=3; //толщина фанеры

difference(){
    polygon([[80,0],[0,50],[80,500],[240,500],[240,580],[0,580]], paths = [[0,1,5,4,3,2]]);
    translate([40,60]) circle(d=4, $fn=50);
    translate([40,260]) circle(d=4, $fn=50);
    translate([40,480]) circle(d=4, $fn=50);
    translate([125,540]) motor_dyrka();
    translate([220,540]) motor_dyrka();
    translate([80,510])ship_poperek();
    translate([80+67,510])ship_poperek(3dth);
    translate([80+67+80,510])ship_poperek(3dth);
}


module kil()
{
difference(){
    polygon([[90,0],[0,50],[90,580],[0,580]], paths = [[0,1,3,2]]);
    translate([50,60]) circle(d=4, $fn=50);
    translate([50,260]) circle(d=4, $fn=50);
    translate([50,480]) circle(d=4, $fn=50);
}
}


module motor_dyrka()
{
 translate([3,3]) circle(d=3, $fn=50);
 translate([3,-3]) circle(d=3, $fn=50);
 translate([-3,3]) circle(d=3, $fn=50);
 translate([-3,-3]) circle(d=3, $fn=50);
}

module poperek(width){
    union()
    translate([0,3]) polygon([[0,0],[60,0],[60,width-3dth],[0,width-3dth]], paths=[[0,1,2,3]]);
    polygon([[5,0],[5,3],[20,3],[20,0]], paths=[[3,2,1,0]]);
    polygon([[40,0],[40,3],[55,3],[55,0]], paths=[[3,2,1,0]]);
    polygon([[5,width],[5,width+3dth],[20,width+3dth],[20,width]], paths=[[3,2,1,0]]);
    polygon([[40,width],[40,width+3dth],[55,width+3dth],[55,width]], paths=[[3,2,1,0]]);
}

translate([0,-w]) poperek(w);
translate ([-100,0]) kil();

module ship_poperek(thickness)
{
    polygon([[0,5],[thickness,5],[thickness,20],[0,20]], paths=[[3,2,1,0]]);
    polygon([[0,40],[thickness,40],[thickness,55],[0,55]], paths=[[3,2,1,0]]);
}

  